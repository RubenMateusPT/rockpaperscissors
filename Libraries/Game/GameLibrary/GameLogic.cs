﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public class GameLogic
    {
        public int RollDice()
        {
            int totalToMove;

            Random rdn = new Random();

            return totalToMove = rdn.Next(1,7);
        }

        public string[] LadderSnackes(int tokenPosition)
        {
            string[] bonus = new string[3];

            switch (tokenPosition)
            {
                default:
                    bonus[0] = "No Bonus";
                    bonus[1] = "0";
                    bonus[2] = "null";
                    break;
                #region Ladders
                case 2:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "36";
                    bonus[2] = "true";
                    break;
                case 7:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "7";
                    bonus[2] = "true";
                    break;
                case 8:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "23";
                    bonus[2] = "true";
                    break;
                case 15:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "11";
                    bonus[2] = "true";
                    break;
                case 21:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "21";
                    bonus[2] = "true";
                    break;
                case 28:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "56";
                    bonus[2] = "true";
                    break;
                case 36:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "8";
                    bonus[2] = "true";
                    break;
                case 51:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "16";
                    bonus[2] = "true";
                    break;
                case 71:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "20";
                    bonus[2] = "true";
                    break;
                case 78:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "20";
                    bonus[2] = "true";
                    break;
                case 87:
                    bonus[0] = "Up the Ladder!";
                    bonus[1] = "7";
                    bonus[2] = "true";
                    break;
                #endregion

                #region Snackes
                case 16:
                    bonus[0] = "Down the snake!";
                    bonus[1] = "-10";
                    bonus[2] = "false";
                    break;
                case 46:
                    bonus[0] = "Down the snake!";
                    bonus[1] = "-21";
                    bonus[2] = "false";
                    break;
                case 49:
                    bonus[0] = "Down the snake!";
                    bonus[1] = "-38";
                    bonus[2] = "false";
                    break;
                case 62:
                    bonus[0] = "Down the snake!";
                    bonus[1] = "-43";
                    bonus[2] = "false";
                    break;
                case 64:
                    bonus[0] = "Down the snake!";
                    bonus[1] = "-4";
                    bonus[2] = "false";
                    break;
                case 74:
                    bonus[0] = "Down the snake!";
                    bonus[1] = "-21";
                    bonus[2] = "false";
                    break;
                case 89:
                    bonus[0] = "Down the snake!";
                    bonus[1] = "-21";
                    bonus[2] = "false";
                    break;
                case 92:
                    bonus[0] = "Down the snake!";
                    bonus[1] = "-4";
                    bonus[2] = "false";
                    break;
                case 95:
                    bonus[0] = "Down the snake!";
                    bonus[1] = "-20";
                    bonus[2] = "false";
                    break;
                case 99:
                    bonus[0] = "Down the snake!";
                    bonus[1] = "-19";
                    bonus[2] = "false";
                    break;
                #endregion
                case 100:
                    bonus[0] = "WIN'S";
                    bonus[1] = "0";
                    bonus[2] = "winner";
                    break;
            }

            return bonus;
        }
    }
}
