﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public class Player
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int PlayerRank { get; set; }
        public Token Token { get; set; }

        public Player(int ID, string Name, Token Token)
        {
            this.ID = ID;
            this.Name = Name;
            PlayerRank = 1;
            this.Token = Token;
        }
    }
}
