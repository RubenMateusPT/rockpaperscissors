﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public class ServerLogic
    {
        Player Player;
        List<Player> PlayerList;
        GameLogic GameBoard;
        Token PlayerToken;

        int turn = 0;

        int requeiredPlayers;
        public ServerLogic(int requeiredPlayers)
        {
            PlayerList = new List<Player>();
            GameBoard = new GameLogic();
            this.requeiredPlayers = requeiredPlayers;
        }
        private Player GetPlayer(int ID)
        {

            Player playerToSelect = (from _player in PlayerList
                                     where _player.ID == ID
                                     select _player).SingleOrDefault();
            return playerToSelect;
        }

        public string Process(string command)
        {
            string serverResponse = null;

            string[] commands = command.Split('|');

            switch (commands[0].ToLower())
            {
                case "newplayer":
                    {
                        int id = 0;

                        if(PlayerList.Count == 0)
                        {
                            id = 1;
                        }
                        else
                        {
                            id = PlayerList.Max((c) => c.ID) + 1;
                        }


                        switch (id)
                        {
                            case 1:
                                {
                                    PlayerToken = new Token(Color.Blue, 1, 5, 451);
                                }
                                break;

                            case 2:
                                {
                                    PlayerToken = new Token(Color.Green, 1, 5, 476);
                                }
                                break;

                            case 3:
                                {
                                    PlayerToken = new Token(Color.Red, 1, 30, 451);
                                }
                                break;

                            case 4:
                                {
                                    PlayerToken = new Token(Color.Yellow, 1, 30, 476);
                                }
                                break;

                        }

                        serverResponse = $"newid|{id.ToString()}|{commands[1]}|{requeiredPlayers}|{PlayerToken.ToString()}";

                        Player = new Player(id, commands[1],PlayerToken);
                        PlayerList.Add(Player);
                    }
                    break;

                case "getplayerlist":
                    {
                        int id = int.Parse(commands[1]);

                        serverResponse = $"noplayer";

                        Player playerToSend = GetPlayer(id);
                        if(playerToSend != null)
                        {
                            serverResponse = $"playerlist|{playerToSend.ID}|{playerToSend.Name}|{playerToSend.Token.ToString()}";
                        }

                    }
                    break;

                case "chatmessage":
                    {
                        Player messagePlayer = GetPlayer(int.Parse(commands[1]));

                        serverResponse = $"chatMessage|{messagePlayer.Name}|{commands[2]}";
                        
                    }
                    break;

                case "startgame":
                    {
                        if(PlayerList.Count == requeiredPlayers)
                        {
                            serverResponse = "startGame";
                        }
                        else
                        {
                            serverResponse = $"notplayer|{commands[1]}|Not enough Players to start";
                        }
                    }
                    break;

                case "whosfirst":
                    {
                        Random rdn = new Random();
                        int firstPLayer = rdn.Next(1, requeiredPlayers+1);

                        serverResponse = "noplayer";
                        foreach(Player playerFirst in PlayerList)
                        {
                            if(playerFirst.ID == firstPLayer)
                            {
                                turn = playerFirst.ID;
                                serverResponse = $"firstplayer|{playerFirst.ID}|{playerFirst.Name}|{playerFirst.Token.Color.ToKnownColor()}";
                            }
                        }
                    }
                    break;

                case "rolldice":

                    int movePlayer = GameBoard.RollDice();

                    Player playerToMove = GetPlayer(int.Parse(commands[1]));

                    serverResponse = $"gameboard|moveplayer|{playerToMove.ID}|{movePlayer}";

                    break;

                case "checkmyposition":
                    {
                        Player playerWhoBonus = GetPlayer(int.Parse(commands[1]));

                        string[] bonus = GameBoard.LadderSnackes(int.Parse(commands[2]));

                        switch (bonus[2])
                        {
                            case "true":
                                serverResponse = $"gameboard|bonus|{playerWhoBonus.ID.ToString()}|{bonus[0]}|{bonus[1]}";
                                break;
                            case "false":
                                serverResponse = $"gameboard|bonus|{playerWhoBonus.ID.ToString()}|{bonus[0]}|{bonus[1]}";
                                break;
                            case "null":
                                serverResponse = $"gameboard|bonus|{playerWhoBonus.ID.ToString()}|{bonus[0]}|{bonus[1]}";
                                break;
                            case "winner":
                                serverResponse = $"gameboard|bonus|{playerWhoBonus.ID.ToString()}|{bonus[0]}";
                                break;
                        }
                    }
                    break;

                case "nextplayer":

                    if(turn == PlayerList.Count)
                    {
                        turn = 1;
                    }
                    else
                    {
                        turn++;
                    }

                    serverResponse = $"gameboard|youturn|{turn}";
                    break;

            }

            return serverResponse;
        }


    }
}
