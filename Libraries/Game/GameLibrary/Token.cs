﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public class Token
    {
        public Color Color { get; set; }
        public int Position { get; set; }
        public int[] FormPosition { get; set; }

        bool right = true;

        public Token(Color Color,int initialPosition,int initialX,int initialY)
        {
            this.Color = Color;
            Position = initialPosition;
            FormPosition = new int[] { initialX, initialY };
        }

        public void convertPositionToForm(int moveSpaces)
        {
            int initPos = Position;
            int currentPos= initPos;
            Position += moveSpaces;
            

            if(Position > 100)
            {
                Position = 100 -(Position - 100);
            }

            if(initPos < Position)
            {
                for(currentPos = initPos; currentPos < Position; currentPos++)
                {
                    MoveFoward(currentPos);
                }
            }
            else
            {
                for (currentPos = initPos; currentPos > Position; currentPos--)
                {
                    moveBackwards(currentPos);
                }
            }


        }

        private void MoveFoward(int position)
        {
            if (position % 10 == 0 && position != 100)
            {
                FormPosition[1] -= 50;
                if (right)
                {
                    right = false;
                }
                else
                {
                    right = true;
                }
            }
            else if (right)
            {
                FormPosition[0] += 46;
            }
            else
            {
                FormPosition[0] -= 46;
            }
        }

        private void moveBackwards(int position)
        {
            for(int i = 11; i < 100; i+=10) { 
            if (position == i && position != 100)
            {
                FormPosition[1] += 50;
                
                if (right)
                {
                    right = false;
                }
                else
                {
                    right = true;
                }
                    return;
            }
            }
            if (right)
            {
                FormPosition[0] -= 46;
            }
            else
            {
                FormPosition[0] += 46;
            }
        }

        
        public override string ToString()
        {
            return $"{this.Color.ToKnownColor()}|{this.Position.ToString()}|{FormPosition[0].ToString()}|{FormPosition[1].ToString()}";
        }
        public Token convertToToken(string colorConvert,string positionConvert, string initX,string initY)
        {
            Token converted;

            Color convertedColor = Color.FromName(colorConvert);
            int convertedPosition = int.Parse(positionConvert);
            int convertedinix = int.Parse(initX);
            int convertediniy = int.Parse(initY);

            converted = new Token(Color.Blue, convertedPosition, convertedinix, convertediniy);

            return converted;
        }
    }
}
