﻿namespace OnlineLibrarie.ClientSide
{
    using System.Net.Sockets;

    public class ClientProperties
    {
        public Common ClientConnection { get; set; }

        public ClientProperties(Socket ClientSocket){
            ClientConnection = new Common();
            ClientConnection.Socket = ClientSocket;
            }
    }
}
