﻿namespace OnlineLibrarie
{

    using System.Net;
    using System.Net.Sockets;

    public class Common
    {
        public Socket Socket { get; set; }

        public int BufferSize { get { return 1024; } }
        public byte[] Buffer { get; set; }
        public string ReceivedData { get; set; }

    }
}
