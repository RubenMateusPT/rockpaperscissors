﻿namespace OnlineLibrarie.ServerSide
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using OnlineLibrarie.ClientSide;
    using GameLibrary;

    public class ServerController
    {

        private ServerProperties _Server;
        private List<ClientProperties> _ClientList;
        private ServerLogic ServerLogic;

        private int maxPlayers;

        public ServerController()
        {
            _Server = new ServerProperties();
            _Server.ServerConnection.Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _Server.ServerConnection.Buffer = new byte[_Server.ServerConnection.BufferSize];

            _ClientList = new List<ClientProperties>();

            
        }

        public void StartServer(int port, int maxPlayers)
        {
            this.maxPlayers = maxPlayers;
            ServerLogic = new ServerLogic(maxPlayers);

            _Server.ServerConnection.Socket.Bind(new IPEndPoint(IPAddress.Any,port));
            _Server.ServerConnection.Socket.Listen(5);

            //Server starts acepting connections
            _Server.ServerConnection.Socket.BeginAccept(new AsyncCallback(AcceptCallback), null);
        }

        private void AcceptCallback(IAsyncResult ar)
        {

            Socket socket = _Server.ServerConnection.Socket.EndAccept(ar);

            ClientProperties newClient = new ClientProperties(socket);

            if(_ClientList.Count < maxPlayers)
            {
                _ClientList.Add(newClient);

                //Server starts receiving information from client
                newClient.ClientConnection.Socket.BeginReceive(_Server.ServerConnection.Buffer, 0, _Server.ServerConnection.Buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), newClient);

                _Server.ServerConnection.Socket.BeginAccept(new AsyncCallback(AcceptCallback), null);
            }
            else
            {
                newClient.ClientConnection.Socket.Close();
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            ClientProperties client = (ClientProperties)ar.AsyncState;

            int receivedDataSize = client.ClientConnection.Socket.EndReceive(ar);
            byte[] receivedData = new byte[receivedDataSize];

            Array.Copy(_Server.ServerConnection.Buffer, receivedData, receivedDataSize);

            string clientRequest = Encoding.ASCII.GetString(receivedData);

            #region ServerLogic
            string serverResponse = null;
            serverResponse = ServerLogic.Process(clientRequest);
            #endregion

            byte[] serverResponseToSend = Encoding.ASCII.GetBytes(serverResponse);

            //Server send response to client
            foreach (ClientProperties player in _ClientList)
            {
                player.ClientConnection.Socket.BeginSend(serverResponseToSend, 0, serverResponseToSend.Length, SocketFlags.None, new AsyncCallback(SendCallBack), player);

            }


            //Server starts receiving again from client
            client.ClientConnection.Socket.BeginReceive(_Server.ServerConnection.Buffer, 0, _Server.ServerConnection.Buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), client);
        }

        private void SendCallBack(IAsyncResult ar)
        {
            ClientProperties client = (ClientProperties)ar.AsyncState;

            client.ClientConnection.Socket.EndSend(ar);
        }

        public void CloseConnections()
        {
            //_Server.ServerConnection.Socket.Close();
        }
    }
}
