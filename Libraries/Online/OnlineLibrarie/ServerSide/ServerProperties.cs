﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrarie.ServerSide
{
    public class ServerProperties
    {
        public Common ServerConnection { get; set; }

        public ServerProperties()
        {
            ServerConnection = new Common();
        }
    }
}
