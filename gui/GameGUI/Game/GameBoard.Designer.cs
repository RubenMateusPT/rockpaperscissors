﻿namespace GameGUI.Game
{
    partial class GameBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chatTextbox = new System.Windows.Forms.TextBox();
            this.chatMessageTextbox = new System.Windows.Forms.TextBox();
            this.sendMessageButton = new System.Windows.Forms.Button();
            this.chatGroupbox = new System.Windows.Forms.GroupBox();
            this.gameBoardGroupbox = new System.Windows.Forms.GroupBox();
            this.startGameButton = new System.Windows.Forms.Button();
            this.leaderboardGroupbox = new System.Windows.Forms.GroupBox();
            this.serverMessage = new System.Windows.Forms.Label();
            this.leaderboardFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.rollDiceButton = new System.Windows.Forms.Button();
            this.boardPictureBox = new System.Windows.Forms.PictureBox();
            this.chatGroupbox.SuspendLayout();
            this.gameBoardGroupbox.SuspendLayout();
            this.leaderboardGroupbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boardPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // chatTextbox
            // 
            this.chatTextbox.BackColor = System.Drawing.Color.White;
            this.chatTextbox.ForeColor = System.Drawing.Color.Black;
            this.chatTextbox.Location = new System.Drawing.Point(19, 21);
            this.chatTextbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chatTextbox.Multiline = true;
            this.chatTextbox.Name = "chatTextbox";
            this.chatTextbox.ReadOnly = true;
            this.chatTextbox.Size = new System.Drawing.Size(228, 589);
            this.chatTextbox.TabIndex = 0;
            // 
            // chatMessageTextbox
            // 
            this.chatMessageTextbox.Location = new System.Drawing.Point(19, 615);
            this.chatMessageTextbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chatMessageTextbox.Name = "chatMessageTextbox";
            this.chatMessageTextbox.Size = new System.Drawing.Size(148, 22);
            this.chatMessageTextbox.TabIndex = 1;
            this.chatMessageTextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chatMessageTextbox_KeyDown);
            this.chatMessageTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chatMessageTextbox_KeyPress);
            // 
            // sendMessageButton
            // 
            this.sendMessageButton.Location = new System.Drawing.Point(173, 615);
            this.sendMessageButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sendMessageButton.Name = "sendMessageButton";
            this.sendMessageButton.Size = new System.Drawing.Size(75, 23);
            this.sendMessageButton.TabIndex = 2;
            this.sendMessageButton.Text = "Send";
            this.sendMessageButton.UseVisualStyleBackColor = true;
            this.sendMessageButton.Click += new System.EventHandler(this.sendMessageButton_Click);
            // 
            // chatGroupbox
            // 
            this.chatGroupbox.Controls.Add(this.chatTextbox);
            this.chatGroupbox.Controls.Add(this.sendMessageButton);
            this.chatGroupbox.Controls.Add(this.chatMessageTextbox);
            this.chatGroupbox.Location = new System.Drawing.Point(825, 12);
            this.chatGroupbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chatGroupbox.Name = "chatGroupbox";
            this.chatGroupbox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chatGroupbox.Size = new System.Drawing.Size(260, 649);
            this.chatGroupbox.TabIndex = 3;
            this.chatGroupbox.TabStop = false;
            this.chatGroupbox.Text = "Chat";
            // 
            // gameBoardGroupbox
            // 
            this.gameBoardGroupbox.Controls.Add(this.startGameButton);
            this.gameBoardGroupbox.Controls.Add(this.leaderboardGroupbox);
            this.gameBoardGroupbox.Controls.Add(this.boardPictureBox);
            this.gameBoardGroupbox.Location = new System.Drawing.Point(12, 12);
            this.gameBoardGroupbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gameBoardGroupbox.Name = "gameBoardGroupbox";
            this.gameBoardGroupbox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gameBoardGroupbox.Size = new System.Drawing.Size(807, 649);
            this.gameBoardGroupbox.TabIndex = 4;
            this.gameBoardGroupbox.TabStop = false;
            this.gameBoardGroupbox.Text = "Board";
            // 
            // startGameButton
            // 
            this.startGameButton.Location = new System.Drawing.Point(349, 281);
            this.startGameButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.startGameButton.Name = "startGameButton";
            this.startGameButton.Size = new System.Drawing.Size(147, 69);
            this.startGameButton.TabIndex = 2;
            this.startGameButton.Text = "Start Game";
            this.startGameButton.UseVisualStyleBackColor = true;
            this.startGameButton.Visible = false;
            this.startGameButton.Click += new System.EventHandler(this.startGameButton_Click);
            // 
            // leaderboardGroupbox
            // 
            this.leaderboardGroupbox.Controls.Add(this.serverMessage);
            this.leaderboardGroupbox.Controls.Add(this.leaderboardFlowPanel);
            this.leaderboardGroupbox.Controls.Add(this.rollDiceButton);
            this.leaderboardGroupbox.Location = new System.Drawing.Point(629, 21);
            this.leaderboardGroupbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.leaderboardGroupbox.Name = "leaderboardGroupbox";
            this.leaderboardGroupbox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.leaderboardGroupbox.Size = new System.Drawing.Size(172, 617);
            this.leaderboardGroupbox.TabIndex = 1;
            this.leaderboardGroupbox.TabStop = false;
            this.leaderboardGroupbox.Text = "Leaderboard";
            this.leaderboardGroupbox.Visible = false;
            // 
            // serverMessage
            // 
            this.serverMessage.BackColor = System.Drawing.Color.White;
            this.serverMessage.Location = new System.Drawing.Point(5, 428);
            this.serverMessage.Name = "serverMessage";
            this.serverMessage.Size = new System.Drawing.Size(160, 116);
            this.serverMessage.TabIndex = 5;
            this.serverMessage.Text = "RaymanX11 Turn";
            this.serverMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // leaderboardFlowPanel
            // 
            this.leaderboardFlowPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.leaderboardFlowPanel.Location = new System.Drawing.Point(9, 21);
            this.leaderboardFlowPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.leaderboardFlowPanel.Name = "leaderboardFlowPanel";
            this.leaderboardFlowPanel.Size = new System.Drawing.Size(157, 385);
            this.leaderboardFlowPanel.TabIndex = 4;
            this.leaderboardFlowPanel.WrapContents = false;
            // 
            // rollDiceButton
            // 
            this.rollDiceButton.Location = new System.Drawing.Point(5, 564);
            this.rollDiceButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rollDiceButton.Name = "rollDiceButton";
            this.rollDiceButton.Size = new System.Drawing.Size(160, 47);
            this.rollDiceButton.TabIndex = 3;
            this.rollDiceButton.Text = "Roll Dice";
            this.rollDiceButton.UseVisualStyleBackColor = true;
            this.rollDiceButton.Visible = false;
            this.rollDiceButton.Click += new System.EventHandler(this.rollDiceButton_Click);
            // 
            // boardPictureBox
            // 
            this.boardPictureBox.Image = global::GameGUI.Properties.Resources.Board;
            this.boardPictureBox.ImageLocation = ".\\Board.jpg";
            this.boardPictureBox.Location = new System.Drawing.Point(5, 21);
            this.boardPictureBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.boardPictureBox.Name = "boardPictureBox";
            this.boardPictureBox.Size = new System.Drawing.Size(617, 617);
            this.boardPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.boardPictureBox.TabIndex = 0;
            this.boardPictureBox.TabStop = false;
            this.boardPictureBox.Visible = false;
            // 
            // GameBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 671);
            this.Controls.Add(this.gameBoardGroupbox);
            this.Controls.Add(this.chatGroupbox);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "GameBoard";
            this.Text = "GameBoard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameBoard_FormClosing);
            this.chatGroupbox.ResumeLayout(false);
            this.chatGroupbox.PerformLayout();
            this.gameBoardGroupbox.ResumeLayout(false);
            this.leaderboardGroupbox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.boardPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox chatTextbox;
        private System.Windows.Forms.TextBox chatMessageTextbox;
        private System.Windows.Forms.Button sendMessageButton;
        private System.Windows.Forms.GroupBox chatGroupbox;
        private System.Windows.Forms.GroupBox gameBoardGroupbox;
        private System.Windows.Forms.PictureBox boardPictureBox;
        private System.Windows.Forms.GroupBox leaderboardGroupbox;
        private System.Windows.Forms.Button startGameButton;
        private System.Windows.Forms.Button rollDiceButton;
        private System.Windows.Forms.FlowLayoutPanel leaderboardFlowPanel;
        private System.Windows.Forms.Label serverMessage;
    }
}