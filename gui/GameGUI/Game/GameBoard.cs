﻿using OnlineLibrarie.ClientSide;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameLibrary;
using System.Threading;

namespace GameGUI.Game
{

    public partial class GameBoard : Form
    {
        ClientProperties _Client;

        List<Player> PlayerList;
        Player Player;
        Player Self;

        Token PlayerToken;
        List<GameToken> BoardToken;

        int GameTurn = 1;


        public GameBoard(string MyName,IPAddress hostIP, int hostPort)
        {
            BoardToken = new List<GameToken>();

            PlayerList = new List<Player>();
            Self = new Player(0, "none",null);

            _Client = new ClientProperties(null);
            _Client.ClientConnection.Buffer = new byte[_Client.ClientConnection.BufferSize];

            InitializeComponent();
            //boardPictureBox.ImageLocation = @"..\Board.jpg";

            ConnectToServer(hostIP, hostPort);
            Thread.Sleep(TimeSpan.FromSeconds(2));
            
            SendToServer($"newPlayer|{MyName}");

        }

        #region JoinServer
        /// <summary>
        /// Tries to connecto to server
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        private void ConnectToServer(IPAddress ipAddress, int port)
        {

            IPEndPoint serverEP = new IPEndPoint(ipAddress, port);

            //Create client socket base on server
            _Client.ClientConnection.Socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            //Starts to connect to server
            _Client.ClientConnection.Socket.BeginConnect(serverEP, new AsyncCallback(ConnectCallBack), _Client);

            
        }

        private void ConnectCallBack(IAsyncResult ar)
        {

            ClientProperties server = (ClientProperties)ar.AsyncState;

            _Client.ClientConnection.Socket.EndConnect(ar);

            try
            {
                server.ClientConnection.Socket.BeginReceive(_Client.ClientConnection.Buffer, 0, _Client.ClientConnection.Buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), server);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                MessageBox.Show("Connection lost from server");
                this.Invoke(
                    new Action(
                        () =>
                        {
                            this.Close();
                        }
                        )
                        );
            }
        }

        /// <summary>
        /// Starts receiving server messages and decrypts them
        /// </summary>
        /// <param name="ar"></param>
        private void ReceiveCallback(IAsyncResult ar)
        {
            ClientProperties server = (ClientProperties)ar.AsyncState;

            //Receives and Decrypts Server Message
            #region Server Message
            int receivedDataSize = server.ClientConnection.Socket.EndReceive(ar);
            byte[] receivedData = new byte[receivedDataSize];

            Array.Copy(_Client.ClientConnection.Buffer, receivedData, receivedDataSize);

            string serverResponse = Encoding.ASCII.GetString(receivedData);
            #endregion

            //INSERT FORM CODE HERE
            FormLogic(serverResponse);
            // END OF FORM CODE

            //Starts listening to server requests
            server.ClientConnection.Socket.BeginReceive(_Client.ClientConnection.Buffer, 0, _Client.ClientConnection.Buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), server);
        }

        /// <summary>
        /// Clients starts to send a message to server
        /// </summary>
        /// <param name="request">Client Message</param>
        private void SendToServer(string request)
        {

            byte[] requestoToSend = Encoding.ASCII.GetBytes(request);

            _Client.ClientConnection.Socket.BeginSend(requestoToSend, 0, requestoToSend.Length, SocketFlags.None, new AsyncCallback(SendCallback), _Client);

        }

        /// <summary>
        /// Ends message to send to server
        /// </summary>
        /// <param name="ar"></param>
        private void SendCallback(IAsyncResult ar)
        {
            ClientProperties server = (ClientProperties)ar.AsyncState;

            server.ClientConnection.Socket.EndSend(ar);
        }
        #endregion

        #region Chat
        private void ChatUpdate(string message)
        {
            this.Invoke(
                new Action(
                    () =>
                    {
                        chatTextbox.AppendText(message + Environment.NewLine);
                    }
                )
             );
        }

        private void sendMessageButton_Click(object sender, EventArgs e)
        {
            SendToServer($"chatMessage|{Self.ID}|{chatMessageTextbox.Text}");
            chatMessageTextbox.Text = "";
        }

        private void chatMessageTextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void chatMessageTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendToServer($"chatMessage|{Self.ID}|{chatMessageTextbox.Text}");
                chatMessageTextbox.Text = "";
            }
        }
        #endregion

        #region GameButtons
        private void startGameButton_Click(object sender, EventArgs e)
        {
            SendToServer($"startGame|{Self.ID}");
        }

        private void rollDiceButton_Click(object sender, EventArgs e)
        {
            rollDiceButton.Visible = false;
            rollDiceButton.Enabled = false;

            SendToServer($"rollDice|{Self.ID}");
        }
        #endregion

        private Player GetPlayer(int ID)
        {

            Player playerToSelect = (from _player in PlayerList
                                     where _player.ID == ID
                                     select _player).SingleOrDefault();
            return playerToSelect;
        }

        /// <summary>
        /// Receives server response and updates the form graphics/logic
        /// </summary>
        /// <param name="serverResponse">Server Message</param>
        private void FormLogic(string serverResponse)
        {

            string[] serverRequest = serverResponse.Split('|');
            switch (serverRequest[0].ToLower())
            {
                case "noplayer":
                    //does nothing
                    break;
                case "notplayer":
                    {
                        if(Self.ID == int.Parse(serverRequest[1]))
                        {
                            MessageBox.Show(serverRequest[2]);
                        }
                    }
                    break;

                case "newid":
                    { 
                        int id = int.Parse(serverRequest[1]);
                        string PlayerName = serverRequest[2];

                        PlayerToken = new Token(Color.FromName(serverRequest[4]), int.Parse(serverRequest[5]), int.Parse(serverRequest[6]), int.Parse(serverRequest[7]));
                        
                        Player = new Player(id, PlayerName,PlayerToken);

                        if (Self.ID == 0)
                        {
                            Self.ID = Player.ID;
                            Self.Name = Player.Name;
                            Self.Token = PlayerToken;
                        }
                        
                        ChatUpdate( $"A new player has choined:" + Environment.NewLine + $"{id} : {PlayerName.ToUpper()}");

                        PlayerList.Add(Player);

                        if (Self.ID > PlayerList.Count())
                        {
                            SendToServer($"getPlayerList|1");
                        }

                        if (Self.ID == 1 && PlayerList.Count == int.Parse(serverRequest[3]))
                        {
                            this.Invoke(new Action(() => startGameButton.Visible = true));
                        }

                    }
                    break;

                case "playerlist":
                    {
                        if(Self.ID > PlayerList.Count())
                        {
                            PlayerToken = new Token(Color.FromName(serverRequest[3]), int.Parse(serverRequest[4]), int.Parse(serverRequest[5]), int.Parse(serverRequest[6]));
                            Player newPlayer = new Player(int.Parse(serverRequest[1]), serverRequest[2],PlayerToken);

                            foreach (Player player in PlayerList)
                            {
                                if (int.Parse(serverRequest[1]) == player.ID)
                                {
                                    newPlayer = null;
                                }
                            }

                            if (int.Parse(serverRequest[1]) != Self.ID && newPlayer != null)
                            {
                                PlayerList.Add(newPlayer);
                                SendToServer($"getPlayerList|{PlayerList.Count.ToString()}");
                            }
                        }
                    }
                    break;

                case "chatmessage":
                    {
                        ChatUpdate($"{serverRequest[1]}: {serverRequest[2]}");
                    }
                    break;

                case "startgame":
                    {

                        foreach (Player player in PlayerList)
                        {
                            PlayerLeaderBoard playerBoard = new PlayerLeaderBoard(player.Name,player.PlayerRank,player.Token.Position, player.Token.Color);

                            this.Invoke(new Action(() => leaderboardFlowPanel.Controls.Add(playerBoard)));
                            

                            GameToken token = new GameToken(player);

                            this.Invoke(new Action(() =>
                            {
                                boardPictureBox.Controls.Add(token);
                                BoardToken.Add(token);
                                token.Location = new Point(player.Token.FormPosition[0], player.Token.FormPosition[1]);
                            }));
                            
                        }

                        this.Invoke(new Action(() => {
                            startGameButton.Visible = false;
                            boardPictureBox.Visible = true;
                            leaderboardGroupbox.Visible = true;
                                                }));

                        if(Self.ID == GameTurn){
                            SendToServer("whosFirst");
                        }
                    }
                    break;

                case "firstplayer":
                    {
                        GameTurn = int.Parse(serverRequest[1]);

                        Player receivedPlayer = GetPlayer(int.Parse(serverRequest[1]));

                        if (Self.ID == receivedPlayer.ID)
                        {
                            this.Invoke(new Action(() => { rollDiceButton.Visible = true; serverMessage.Text = $"{serverRequest[2]} Turn"; serverMessage.ForeColor = Color.FromName(serverRequest[3]); }));
                        }
                        else
                        {
                            this.Invoke(new Action(() => { serverMessage.Text = $"{serverRequest[2]} Turn"; serverMessage.ForeColor = Color.FromName(serverRequest[3]); }));
                        }

                        
                    }
                    break;

                case "gameboard":
                    {
                        switch (serverRequest[1])
                        {
                            case "moveplayer":
                                {
                                    Player playerToMove = GetPlayer(int.Parse(serverRequest[2]));

                                
                                    playerToMove.Token.convertPositionToForm(int.Parse(serverRequest[3]));

                                    MovePlayerForm(playerToMove);

                                    this.Invoke(new Action(() =>
                                    {
                                        serverMessage.Text = $"{playerToMove.Name} moved {serverRequest[3]}";
                                        serverMessage.ForeColor = playerToMove.Token.Color;

                                    }));
                                    Thread.Sleep(TimeSpan.FromSeconds(1));

                                    if (Self.ID == GameTurn)
                                    {
                                        SendToServer($"checkmyposition|{playerToMove.ID.ToString()}|{playerToMove.Token.Position}");
                                    }
                                    }
                                break;

                            case "bonus":
                                {
                                    Player playerBonus = GetPlayer(int.Parse(serverRequest[2]));

                                    
                                    switch (serverRequest[3])
                                    {
                                        case "No Bonus":

                                            UpdateLeaderBoardView();

                                            if (Self.ID == GameTurn)
                                            {
                                                SendToServer("nextplayer");
                                            }
                                            break;

                                        case "Up the Ladder!":
                                            playerBonus.Token.convertPositionToForm(int.Parse(serverRequest[4]));

                                            this.Invoke(new Action(() =>
                                            {
                                            serverMessage.Text = $"{playerBonus.Name} got lucky!";
                                            
                                            }));
                                            Thread.Sleep(TimeSpan.FromSeconds(1));
                                            MovePlayerForm(playerBonus);

                                            UpdateLeaderBoardView();

                                            if (Self.ID == GameTurn)
                                            {
                                                SendToServer("nextplayer");
                                            }
                                            break;

                                        case "Down the snake!":
                                            playerBonus.Token.convertPositionToForm(int.Parse(serverRequest[4]));

                                            this.Invoke(new Action(() =>
                                            {
                                            serverMessage.Text = $"{playerBonus.Name} got bitten by a sanke!";
                                            
                                            }));
                                            Thread.Sleep(TimeSpan.FromSeconds(1));
                                            MovePlayerForm(playerBonus);

                                            UpdateLeaderBoardView();

                                            if (Self.ID == GameTurn)
                                            {
                                                SendToServer("nextplayer");
                                            }

                                            break;

                                        case "WIN'S":

                                            UpdateLeaderBoardView();

                                            this.Invoke(new Action(() =>
                                            {
                                                serverMessage.Text = $"{playerBonus.Name} has won the game!";
                                                MessageBox.Show($"{playerBonus.Name} has WON! \nThanks for playing!");
                                               
                                                this.Close();
                                            }));

                                            
                                            break;
                                    }
                                    

                                }
                                break;

                            case "youturn":
                                {

                                    Player playerTurn = GetPlayer(int.Parse(serverRequest[2]));
                                    GameTurn = playerTurn.ID;
                                    if (Self.ID == playerTurn.ID)
                                    {
                                        this.Invoke(new Action(() =>
                                        {

                                            Thread.Sleep(2);
                                            rollDiceButton.Visible = true;
                                            rollDiceButton.Enabled = true;

                                        }));
                                    }

                                    this.Invoke(new Action(() => { serverMessage.Text = $"{playerTurn.Name}, It's your turn!"; serverMessage.ForeColor = playerTurn.Token.Color; }));
                                }
                                break;
                        }
                    }
                    break;
            }

            
        }

        #region From Graphs
        public void UpdateLeaderBoardView()
        {

            UpdateScores();   

            this.Invoke(new Action(() => leaderboardFlowPanel.Controls.Clear()));
            foreach (Player player in PlayerList)
            {
                PlayerLeaderBoard playerBoard = new PlayerLeaderBoard(player.Name, player.PlayerRank, player.Token.Position,player.Token.Color);
                this.Invoke(new Action(() => leaderboardFlowPanel.Controls.Add(playerBoard)));
            }
        }

        public void UpdateScores()
        {

            int[] playerPosition = new int[PlayerList.Count];

            for (int i = 0; i < playerPosition.GetLength(0); i++)
            {
                playerPosition[i] = GetPlayer(i + 1).Token.Position;
            }

            int[] sorteByPosition = playerPosition.OrderByDescending(i => i).ToArray();

            foreach (Player player in PlayerList)
            {
                if (player.Token.Position == sorteByPosition[0])
                {
                    player.PlayerRank = 1;
                }

                if (PlayerList.Count >= 2)
                {
                    if (player.Token.Position == sorteByPosition[1])
                    {
                        player.PlayerRank = 2;
                    }
                }

                if (PlayerList.Count >= 3)
                {
                    if (player.Token.Position == sorteByPosition[2])
                    {
                        player.PlayerRank = 3;
                    }
                }

                if (PlayerList.Count >= 4)
                {
                    if (player.Token.Position == sorteByPosition[3])
                    {
                        player.PlayerRank = 4;
                    }
                }
                }
            
        }
        public void MovePlayerForm(Player playerToMove)
        {
            foreach(GameToken token in BoardToken)
            {
                if(playerToMove.ID == token.ID)
                {
                    this.Invoke(new Action(() =>
                    token.Location = new Point(playerToMove.Token.FormPosition[0], playerToMove.Token.FormPosition[1])
                    ));
                }
            }

            }


        #endregion

        private void GameBoard_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
    }
}
