﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameLibrary;

namespace GameGUI.Game
{
    public partial class GameToken : UserControl
    {

        public int ID { get; set; }
        public GameToken()
        {
            InitializeComponent();
        }

        public GameToken(Player PLayerToken)
        {
            InitializeComponent();
            ForeColor = PLayerToken.Token.Color;
            this.ID = PLayerToken.ID;
        }
    }
}
