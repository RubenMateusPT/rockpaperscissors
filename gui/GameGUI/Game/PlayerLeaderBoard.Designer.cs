﻿namespace GameGUI.Game
{
    partial class PlayerLeaderBoard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tokenPositionLabel = new System.Windows.Forms.Label();
            this.playerPositionLabel = new System.Windows.Forms.Label();
            this.playerNameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tokenPositionLabel
            // 
            this.tokenPositionLabel.Location = new System.Drawing.Point(58, 32);
            this.tokenPositionLabel.Name = "tokenPositionLabel";
            this.tokenPositionLabel.Size = new System.Drawing.Size(94, 23);
            this.tokenPositionLabel.TabIndex = 5;
            this.tokenPositionLabel.Text = "Position: 100";
            this.tokenPositionLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // playerPositionLabel
            // 
            this.playerPositionLabel.Location = new System.Drawing.Point(4, 32);
            this.playerPositionLabel.Name = "playerPositionLabel";
            this.playerPositionLabel.Size = new System.Drawing.Size(25, 23);
            this.playerPositionLabel.TabIndex = 4;
            this.playerPositionLabel.Text = "4º";
            // 
            // playerNameLabel
            // 
            this.playerNameLabel.Location = new System.Drawing.Point(3, 9);
            this.playerNameLabel.Name = "playerNameLabel";
            this.playerNameLabel.Size = new System.Drawing.Size(149, 23);
            this.playerNameLabel.TabIndex = 3;
            this.playerNameLabel.Text = "LOLOLOLOLOLOLOL";
            // 
            // PlayerLeaderBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tokenPositionLabel);
            this.Controls.Add(this.playerPositionLabel);
            this.Controls.Add(this.playerNameLabel);
            this.Name = "PlayerLeaderBoard";
            this.Size = new System.Drawing.Size(154, 56);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label tokenPositionLabel;
        private System.Windows.Forms.Label playerPositionLabel;
        private System.Windows.Forms.Label playerNameLabel;
    }
}
