﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameLibrary;

namespace GameGUI.Game
{
    public partial class PlayerLeaderBoard : UserControl
    {
        public PlayerLeaderBoard(string playerName,int playerLeaderboard, int tokenPosition,Color textColor)
        {
            InitializeComponent();

            playerNameLabel.Text = playerName;
            playerNameLabel.ForeColor = textColor;
            playerPositionLabel.Text = playerLeaderboard.ToString() + @"º";
            tokenPositionLabel.Text = "Position: "+ tokenPosition.ToString();
        }
    }
}
