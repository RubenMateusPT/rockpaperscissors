﻿namespace GameGUI.HostJoin
{
    partial class HostGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.portNumeric = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.playerNameTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.hostButton = new System.Windows.Forms.Button();
            this.numberPlayersNumeric = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.portNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberPlayersNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // portNumeric
            // 
            this.portNumeric.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.portNumeric.Location = new System.Drawing.Point(213, 16);
            this.portNumeric.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.portNumeric.Minimum = new decimal(new int[] {
            1025,
            0,
            0,
            0});
            this.portNumeric.Name = "portNumeric";
            this.portNumeric.Size = new System.Drawing.Size(120, 38);
            this.portNumeric.TabIndex = 0;
            this.portNumeric.Value = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Player Name:";
            // 
            // playerNameTextbox
            // 
            this.playerNameTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerNameTextbox.Location = new System.Drawing.Point(213, 125);
            this.playerNameTextbox.MaxLength = 15;
            this.playerNameTextbox.Name = "playerNameTextbox";
            this.playerNameTextbox.Size = new System.Drawing.Size(296, 38);
            this.playerNameTextbox.TabIndex = 2;
            this.playerNameTextbox.TextChanged += new System.EventHandler(this.playerNameTextbox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(33, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 32);
            this.label2.TabIndex = 3;
            this.label2.Text = "Server Port:";
            // 
            // hostButton
            // 
            this.hostButton.Enabled = false;
            this.hostButton.Location = new System.Drawing.Point(356, 12);
            this.hostButton.Name = "hostButton";
            this.hostButton.Size = new System.Drawing.Size(153, 96);
            this.hostButton.TabIndex = 4;
            this.hostButton.Text = "Host";
            this.hostButton.UseVisualStyleBackColor = true;
            this.hostButton.Click += new System.EventHandler(this.hostButton_Click);
            // 
            // numberPlayersNumeric
            // 
            this.numberPlayersNumeric.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberPlayersNumeric.Location = new System.Drawing.Point(213, 70);
            this.numberPlayersNumeric.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numberPlayersNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberPlayersNumeric.Name = "numberPlayersNumeric";
            this.numberPlayersNumeric.Size = new System.Drawing.Size(120, 38);
            this.numberPlayersNumeric.TabIndex = 5;
            this.numberPlayersNumeric.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(43, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 32);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nº Players:";
            // 
            // HostGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 169);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numberPlayersNumeric);
            this.Controls.Add(this.hostButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.playerNameTextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.portNumeric);
            this.Name = "HostGame";
            this.Text = "HostGame";
            ((System.ComponentModel.ISupportInitialize)(this.portNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberPlayersNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown portNumeric;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox playerNameTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button hostButton;
        private System.Windows.Forms.NumericUpDown numberPlayersNumeric;
        private System.Windows.Forms.Label label3;
    }
}