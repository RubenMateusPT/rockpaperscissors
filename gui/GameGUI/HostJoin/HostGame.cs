﻿using GameGUI.Game;
using OnlineLibrarie.ServerSide;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameGUI.HostJoin
{
    public partial class HostGame : Form
    {
        ServerController newServer;

        public HostGame()
        {
            InitializeComponent();
        }

        private void playerNameTextbox_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(playerNameTextbox.Text))
            {
                hostButton.Enabled = false;
            }
            else
            {
                hostButton.Enabled = true;
            }
        }

        private void hostButton_Click(object sender, EventArgs e)
        {
            if(portNumeric.Value < 1025 || portNumeric.Value > 65535 || string.IsNullOrEmpty(playerNameTextbox.Text))
            {
                MessageBox.Show("Invaladid Port or Nickname");
            }
            else
            {
                newServer = new ServerController();
                newServer.StartServer((int)portNumeric.Value, (int)numberPlayersNumeric.Value);



                GameBoard frm = new GameBoard(playerNameTextbox.Text,IPAddress.Parse("127.0.0.1"),(int)portNumeric.Value);
                frm.ShowDialog();
                newServer.CloseConnections();
                this.Close();

            }
        }
    }
}
