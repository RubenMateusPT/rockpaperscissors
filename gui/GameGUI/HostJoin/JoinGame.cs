﻿using GameGUI.Game;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameGUI.HostJoin
{
    public partial class JoinGame : Form
    {

        public JoinGame()
        {
            InitializeComponent();
        }

        private void playerNameTextbox_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(playerNameTextbox.Text) && string.IsNullOrEmpty(serverIPTextbox.Text))
            {
                joinButton.Enabled = false;
            }
            else
            {
                joinButton.Enabled = true;
            }
        }

        private void serverIPTextbox_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(playerNameTextbox.Text) && string.IsNullOrEmpty(serverIPTextbox.Text))
            {
                joinButton.Enabled = false;
            }
            else
            {
                joinButton.Enabled = true;
            }
        }

        private void joinButton_Click(object sender, EventArgs e)
        {
            if (portNumeric.Value < 1025 || portNumeric.Value > 65535 || string.IsNullOrEmpty(playerNameTextbox.Text) || string.IsNullOrEmpty(serverIPTextbox.Text) || serverIPTextbox.Text.Split('.').Length != 4 || serverIPTextbox.Text.Contains(' ') || !IPAddress.TryParse(serverIPTextbox.Text, out IPAddress serverIP))
            {
                MessageBox.Show("Invaladid IP / Port / Nickname");
            }
            else
            {

                GameBoard frm = new GameBoard(playerNameTextbox.Text, serverIP, (int)portNumeric.Value);
                frm.ShowDialog();
                this.Close();

            }
        }
    }
}
