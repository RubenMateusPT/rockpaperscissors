﻿namespace GameGUI
{
    using System;
    using System.Windows.Forms;
    using System.Net;
    using GameGUI.Main;
    using GameGUI.HostJoin;

    public partial class MainMenu : Form
    {

        public MainMenu()
        {
            InitializeComponent();
        }

        private void createGameButton_Click(object sender, EventArgs e)
        {

            HostGame frm = new HostGame();
            frm.ShowDialog();
        }

        private void joinGameButton_Click(object sender, EventArgs e)
        {
            JoinGame frm = new JoinGame();
            frm.ShowDialog();
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            About frm = new About();
            frm.ShowDialog();
        }
        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
    }
}
